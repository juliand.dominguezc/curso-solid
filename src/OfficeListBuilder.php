<?php declare(strict_types = 1);

namespace Drupal\taller_solid;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of offices.
 */
final class OfficeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['code'] = $this->t('Código');
    $header['label'] = $this->t('Descripción');
    $header['address'] = $this->t('Dirección');
    $header['nui'] = $this->t('Identificación');
    $header['currency_id'] = $this->t('Moneda');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\taller_solid\Contracts\Entities\OfficeInterface $entity */
    $row['code'] = $entity->getCode();
    $row['label'] = $entity->label();
    $row['address'] = $entity->getAddress();
    $row['nui'] = $entity->getNui();
    $row['currency_id'] = $entity->getCurrency();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEntityIds(): int|array {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();
    $currency_id = $request->get('currency_id') ?? 0;
    if ($currency_id) {
      $query->condition('currency_id', $currency_id);
    }

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    $build['form'] = \Drupal::formBuilder()
      ->getForm('Drupal\taller_solid\Form\CurrencyFilterForm');
    $build += parent::render();
    return $build;
  }
}
