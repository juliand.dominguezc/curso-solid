<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\Utils;

use Drupal\taller_solid\Contracts\Entities\OfficeInterface;

interface OfficeUtilsInterface {

  /**
   * Función encargada de dar formato a la respuesta de la consulta de oficinas.
   *
   * @param \Drupal\taller_solid\Entity\Office $office
   *
   * @return array
   */
  public function formatOffice(OfficeInterface $office): array;

}
