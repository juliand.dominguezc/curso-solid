<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\Db;

interface QueryUtilsInterface {

  /**
   * Función que retorna todos los elementos almacenados de la entidad deseada
   *
   * @param string $entity_type
   *
   * @return mixed
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityLoadMultiple(string $entity_type): mixed;

  /**
   * Función que retorna un elemento almacenado de la entidad deseada.
   *
   * @param string $entity_type
   * @param string $id
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityById(string $entity_type, string $id): mixed;

}
