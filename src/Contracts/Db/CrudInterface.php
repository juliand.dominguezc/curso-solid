<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\Db;

interface CrudInterface {

  /**
   * Función encargada de crear un elemento en la base de datos
   *
   * @param array $data
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function create(array $data): bool;

  /**
   * Función encargada de eliminar un elemento de la base de datos
   *
   * @param string $entity_type
   * @param string $id
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(string $entity_type, string $id): bool;

  /**
   * Función encargada de actualizar un elemento de la base de datos
   *
   * @param string $id
   * @param array $data
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function update(string $id, array $data): bool;

}
