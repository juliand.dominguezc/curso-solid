<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\RestLogic;

interface OfficeRestLogicInterface {

  /**
   * Función que retorna todas las sucursales u oficinas en formato json
   *
   * @return array
   * @throws \Exception
   */
  public function getAllOffices(): array;

  /**
   * Función que retorna una sucursal u oficina a partir de un ID
   *
   * @param string $id
   *
   * @return array
   * @throws \Exception
   */
  public function getOffice(string $id): array;

  /**
   * Función encargada de crear una sucursal u oficina en la base de datos.
   *
   * @param array $data
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   */
  public function createOffice(array $data): array;

  /**
   * Función encargada de eliminar una sucursal u oficina de la base de datos.
   *
   * @param string $office_id
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteOffice(string $office_id): array;

  /**
   * Función encargada de actualizar una sucursal u oficina en la base de datos.
   *
   * @param string $id
   * @param array $data
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateOffice(string $id, array $data): array;

}
