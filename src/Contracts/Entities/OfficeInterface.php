<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\Entities;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an office entity type.
 */
interface OfficeInterface extends ConfigEntityInterface {

  /**
   * Función que retorna el ID de la entidad
   *
   * @return string
   */
  public function getId(): string;

  /**
   * Función que retorna el nombre del tipo de sucursal u oficina
   *
   * @return string
   * @example "Oficina Bits Colombia"
   */
  public function getLabel(): string;

  /**
   * Función que retorna el código asignado a la sucursal u oficina
   *
   * @return string
   * @example "1002"
   */
  public function getCode(): string;

  /**
   * Función que retorna la dirección de la sucursal u oficina
   *
   * @return string
   * @example "Av. Pueyrredón 123"
   */
  public function getAddress(): string;

  /**
   * Función que retorna el NUI (Número único de identificación) de la sucursal
   * u oficina
   *
   * @return string
   * @example "12345678-9"
   */
  public function getNui(): string;

  /**
   * Función que retorna el código de la moneda
   *
   * @return string
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @example "USD"
   *
   */
  public function getCurrency(): string;

  /**
   * Función que retorna el ID de la moneda
   *
   * @return string
   * @example "usd"
   */
  public function getCurrencyId(): string;

  /**
   * Función que establece el ID de la entidad
   *
   * @param string $id
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setId(string $id): OfficeInterface;

  /**
   * Función que establece el nombre del tipo de sucursal u oficina
   *
   * @param string $label
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setLabel(string $label): OfficeInterface;

  /**
   * Función que establece el código asignado a la sucursal u oficina
   *
   * @param string $code
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setCode(string $code): OfficeInterface;

  /**
   * Función que establece la dirección de la sucursal u oficina
   *
   * @param string $address
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setAddress(string $address): OfficeInterface;

  /**
   * Función que establece el NUI (Número único de identificación) de la
   * sucursal u oficina
   *
   * @param string $nui
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setNui(string $nui): OfficeInterface;

  /**
   * Función que establece el código de la moneda
   *
   * @param string $currency_id
   *
   * @return \Drupal\taller_solid\Contracts\Entities\OfficeInterface
   */
  public function setCurrencyId(string $currency_id): OfficeInterface;

}
