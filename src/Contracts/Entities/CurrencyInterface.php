<?php declare(strict_types=1);

namespace Drupal\taller_solid\Contracts\Entities;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a currency entity type.
 */
interface CurrencyInterface extends ConfigEntityInterface {

  /**
   * Función que retornara el ID de la entidad
   *
   * @return string
   */
  public function getId(): string;

  /**
   * Función que retornara el nombre del tipo de moneda
   *
   * @return string
   * @example "Dollar"
   */
  public function getLabel(): string;

  /**
   * Función que retorna el código del tipo de moneda
   *
   * @return string
   * @example "USD"
   */
  public function getCurrencyCode(): string;

  /**
   * Función que retorna el país al que pertenece la moneda
   *
   * @return string
   * @example "US"
   */
  public function getCountry(): string;

  /**
   * Función que retorna la descripción de la moneda
   *
   * @return string
   * @example "United States Dollar"
   */
  public function getDescription(): string;

  /**
   * Función que establece el ID de la entidad
   *
   * @param string $id
   *
   * @return \Drupal\taller_solid\Contracts\Entities\CurrencyInterface
   */
  public function setId(string $id): CurrencyInterface;

  /**
   * Función que establece el código del tipo de moneda
   *
   * @param string $label
   *
   * @return \Drupal\taller_solid\Contracts\Entities\CurrencyInterface
   */
  public function setLabel(string $label): CurrencyInterface;

  /**
   * Función que establece el código de tipo de moneda
   *
   * @param string $currency_code
   *
   * @return \Drupal\taller_solid\Contracts\Entities\CurrencyInterface
   */
  public function setCurrencyCode(string $currency_code): CurrencyInterface;

  /**
   * Función que establece el país al que pertenece la moneda
   *
   * @param string $country
   *
   * @return \Drupal\taller_solid\Contracts\Entities\CurrencyInterface
   */
  public function setCountry(string $country): CurrencyInterface;

  /**
   * Función que establece la descripción de la moneda
   *
   * @param string $description
   *
   * @return \Drupal\taller_solid\Contracts\Entities\CurrencyInterface
   */
  public function setDescription(string $description): CurrencyInterface;

}
