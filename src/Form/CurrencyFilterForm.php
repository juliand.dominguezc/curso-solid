<?php

namespace Drupal\taller_solid\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CurrencyFilterForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  public static function create(ContainerInterface $container): CurrencyFilterForm {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'currency_filter_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $request = \Drupal::request();
    try {
      $entity_manager = $this->entityTypeManager->getStorage('currency');
      $banks = $entity_manager->loadMultiple();
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      $banks = [];
    }

    $options = [];

    foreach ($banks as $bank) {
      $options[$bank->id()] = $bank->label();
    }

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'clearfix'],
      ],
    ];

    $form['filter']['currency_id'] = [
      '#type' => 'select',
      '#title' => 'Filtrar tipos de moneda',
      '#options' => $options,
      '#default_value' => $request->get('currency_id') ?? 0,
    ];

    $form['actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form-item']],
    ];

    $form['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Filter',
    ];

    if ($request->getQueryString()) {
      $form['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => 'Reset',
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $query = [];
    $currency_id = $form_state->getValue('currency_id') ?? 0;
    if ($currency_id) {
      $query['currency_id'] = $currency_id;
    }
    $form_state->setRedirect('entity.office.collection', $query);
  }

  public function resetForm(array $form, FormStateInterface &$form_state): void {
    $form_state->setRedirect('entity.office.collection');
  }

}
