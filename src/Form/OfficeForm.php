<?php declare(strict_types=1);

namespace Drupal\taller_solid\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taller_solid\Entity\Office;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * office form.
 */
final class OfficeForm extends EntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entity_type_manager;

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container): OfficeForm {
    $instance = parent::create($container);
    $instance->entity_type_manager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $storage = $this->entity_type_manager->getStorage('currency');
    $currencies = $storage->loadMultiple();

    $options = [];
    foreach ($currencies as $currency) {
      $options[$currency->id()] = $currency->label();
    }

    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Código'),
      '#maxlength' => 50,
      '#default_value' => $this->entity->get('code'),
      '#required' => TRUE,
      '#description' => $this->t('Código de la sucursal u oficina'),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre Sucursal'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#description' => $this->t('Nombre de la sucursal u oficina'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [Office::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dirección'),
      '#maxlength' => 50,
      '#default_value' => $this->entity->get('address'),
      '#required' => TRUE,
      '#description' => $this->t('Dirección de la sucursal u oficina'),
    ];

    $form['nui'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identificación NUI'),
      '#maxlength' => 50,
      '#default_value' => $this->entity->get('nui'),
      '#required' => TRUE,
      '#description' => $this->t('Número único de identificación de la sucursal u oficina'),
    ];

    $form['currency_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Móneda'),
      '#options' => $options,
      '#default_value' => $this->entity->get('currency_id'),
      '#description' => $this->t('Banco al que esta asociada la cuenta.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match ($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
