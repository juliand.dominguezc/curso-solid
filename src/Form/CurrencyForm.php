<?php declare(strict_types=1);

namespace Drupal\taller_solid\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taller_solid\Entity\Currency;

/**
 * currency form.
 */
final class CurrencyForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre móneda'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [Currency::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['currency_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Código moneda'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get("currency_code"),
      '#required' => TRUE,
    ];

    $form['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('País'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get("country"),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Descripción'),
      '#default_value' => $this->entity->get('description'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match ($result) {
        \SAVED_NEW => $this->t('Created new currency %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated currency %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
