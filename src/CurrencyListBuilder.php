<?php declare(strict_types = 1);

namespace Drupal\taller_solid;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of currencies.
 */
final class CurrencyListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('Id');
    $header['label'] = $this->t('Nombre moneda');
    $header['currency_code'] = $this->t('Código moneda');
    $header['country'] = $this->t('Pais');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\taller_solid\Contracts\Entities\CurrencyInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['currency_code'] = $entity->getCurrencyCode();
    $row['country'] = $entity->getCountry();
    return $row + parent::buildRow($entity);
  }

}
