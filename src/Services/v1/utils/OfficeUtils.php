<?php

namespace Drupal\taller_solid\Services\v1\utils;

use Drupal\taller_solid\Contracts\Entities\OfficeInterface;
use Drupal\taller_solid\Contracts\Utils\OfficeUtilsInterface;

class OfficeUtils implements OfficeUtilsInterface {

  /**
   * @inheritDoc
   */
  public function formatOffice(OfficeInterface $office): array {
    $data = [
      "code" => $office->getCode(),
      "description" => $office->getLabel(),
      "address" => $office->getAddress(),
      "nui" => $office->getNui(),
      "currency" => $office->getCurrency(),
    ];
    return $data;
  }

}
