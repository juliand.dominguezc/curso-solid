<?php

namespace Drupal\taller_solid\Services\v1;

use Drupal\taller_solid\Contracts\RestLogic\OfficeRestLogicInterface;
use Drupal\taller_solid\Contracts\Utils\OfficeUtilsInterface;
use Drupal\taller_solid\Services\v1\db\OfficeCrud;

class OfficeRestLogic implements OfficeRestLogicInterface {

  const ENTITY_TYPE = 'office';

  /**
   * @param \Drupal\taller_solid\Contracts\Utils\OfficeUtilsInterface $office_utils
   * @param \Drupal\taller_solid\Services\v1\db\OfficeCrud $office_crud
   */
  public function __construct(
    protected OfficeUtilsInterface $office_utils,
    protected OfficeCrud $office_crud,
  ) {}

  /**
   * @inheritDoc
   */
  public function getAllOffices(): array {
    $offices = $this->office_crud->getEntityLoadMultiple(self::ENTITY_TYPE);
    $options = [];
    foreach ($offices as $office) {
      $options[$office->id()] = $this->office_utils->formatOffice($office);
    }
    return $options;
  }

  /**
   * @inheritDoc
   */
  public function getOffice(string $id): array {
    $office = $this->office_crud->getEntityById(self::ENTITY_TYPE, $id);
    if ($office) {
      $response = $this->office_utils->formatOffice($office);
    }
    else {
      $response = ["message" => 'No se encontró la sucursal o oficina solicitada'];
    }

    return  $response;
  }

  /**
   * @inheritDoc
   *
   * @param array $data
   *
   * @return array
   */
  public function createOffice(array $data): array {
    $office = $this->office_crud->create($data);
    if ($office) {
      $message = ['successfully_office_create' => TRUE];
    }
    else {
      $message = ["message" => "No se pudo crear la sucursal o oficina solicitada"];
    }
    return $message;
  }

  /**
   * @inheritDoc
   */
  public function deleteOffice(string $office_id): array {
    $office = $this->office_crud->delete(self::ENTITY_TYPE, $office_id);
    if ($office) {
      $message = ['successfully_office_delete' => TRUE];
    }
    else {
      $message = ["message" => "No se encontró la sucursal u oficina a eliminar con ID: " . $office_id];
    }

    return $message;
  }

  /**
   * @inheritDoc
   */
  public function updateOffice(string $id, array $data):array {
    $office = $this->office_crud->update($id, $data);
    if ($office) {
      $message = ['successfully_office_update' => TRUE];
    }
    else {
      $message = ["message" => "No se encontró la sucursal o oficina a actualizar con ID: " . $id];
    }
    return $message;
  }

}
