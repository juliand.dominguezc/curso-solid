<?php

namespace Drupal\taller_solid\Services\v1\db;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taller_solid\Contracts\Db\QueryUtilsInterface;

class QueryUtils implements QueryUtilsInterface {

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(
    protected EntityTypeManagerInterface $entity_type_manager,
  ) {}

  /**
   * @inheritDoc
   */
  public function getEntityLoadMultiple(string $entity_type): array {
    try {
      $storage = $this->entity_type_manager->getStorage($entity_type);
      $entities = $storage->loadMultiple();
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e->getMessage());
    }
    return $entities;
  }

  /**
   * @inheritDoc
   */
  public function getEntityById(string $entity_type, string $id): mixed {
    try {
      $storage = $this->entity_type_manager->getStorage($entity_type);
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e->getMessage());
    }

    return $storage->load($id);
  }

}
