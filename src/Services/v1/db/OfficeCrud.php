<?php

namespace Drupal\taller_solid\Services\v1\db;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taller_solid\Contracts\Db\CrudInterface;
use Drupal\taller_solid\Services\v1\OfficeRestLogic;

class OfficeCrud extends QueryUtils implements CrudInterface {

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(protected EntityTypeManagerInterface $entity_type_manager) {}

  /**
   * @inheritDoc
   */
  public function create(array $data): bool {
    $data["id"] = strtolower(strtr($data["label"], " ", "_"));
    $office = $this->entity_type_manager->getStorage(OfficeRestLogic::ENTITY_TYPE)
      ->create($data);
    try {
      $office->save();
    }
    catch (EntityStorageException $e) {
      throw new EntityStorageException($e->getMessage());
    }

    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function delete(string $entity_type, string $id): bool {
    $status = FALSE;
    try {
      $office = $this->getEntityById($entity_type, $id);
      if ($office) {
        $office->delete();
        $status = TRUE;
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e->getMessage());
    }
    catch (EntityStorageException $e) {
      throw new EntityStorageException($e->getMessage());
    }

    return $status;
  }

  /**
   * @inheritDoc
   */
  public function update(string $id, array $data): bool {
    $status = FALSE;
    try {
      $office = $this->getEntityById(OfficeRestLogic::ENTITY_TYPE, $id);
      if ($office) {
        foreach ($data as $key => $value) {
          $office->set($key, $value);
        }
        $office->save();
        $status = TRUE;
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e->getMessage());
    }
    catch (EntityStorageException $e) {
      throw new EntityStorageException($e->getMessage());
    }

    return $status;
  }

}
