<?php declare(strict_types=1);

namespace Drupal\taller_solid\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\taller_solid\Contracts\Entities\OfficeInterface;
use Drupal\taller_solid\Form\OfficeForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the office entity type.
 *
 * @ConfigEntityType(
 *   id = "office",
 *   label = @Translation("office"),
 *   label_collection = @Translation("offices"),
 *   label_singular = @Translation("office"),
 *   label_plural = @Translation("offices"),
 *   label_count = @PluralTranslation(
 *     singular = "@count office",
 *     plural = "@count offices",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\taller_solid\OfficeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\taller_solid\Form\OfficeForm",
 *       "edit" = "Drupal\taller_solid\Form\OfficeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "office",
 *   admin_permission = "administer office",
 *   links = {
 *     "collection" = "/admin/structure/office",
 *     "add-form" = "/admin/structure/office/add",
 *     "edit-form" = "/admin/structure/office/{office}",
 *     "delete-form" = "/admin/structure/office/{office}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "code",
 *     "address",
 *     "nui",
 *     "currency_id",
 *   },
 * )
 */
final class Office extends ConfigEntityBase implements OfficeInterface {

  /**
   * The ID.
   */
  protected string $id;

  /**
   * The label.
   * @var string
   */
  protected string $label;

  /**
   * The code.
   * @var string
   */
  protected string $code;

  /**
   * The address.
   * @var string
   */
  protected string $address;

  /**
   * The unique identification number
   * @var string
   */
  protected string $nui;

  /**
   * The currency_id.
   * @var string
   */
  protected string $currency_id;

  /**
   * @inheritDoc
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @inheritDoc
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * @inheritDoc
   */
  public function getCode(): string {
    return $this->code;
  }

  /**
   * @inheritDoc
   */
  public function getAddress(): string {
    return $this->address;
  }

  /**
   * @inheritDoc
   */
  public function getNui(): string {
    return $this->nui;
  }

  /**
   * @inheritDoc
   */
  public function getCurrencyId(): string {
    return $this->currency_id;
  }

  /**
   * @inheritDoc
   */
  public function getCurrency(): string {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager */
    $entityManager = \Drupal::service('entity_type.manager');
    /** @var \Drupal\taller_solid\Entity\Currency $currency */
    try {
      $currency = $entityManager->getStorage('currency')
        ->load($this->getCurrencyId());
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e->getMessage());
    }
    return $currency->getCurrencyCode();
  }

  /**
   * @inheritDoc
   */
  public function setId(string $id): OfficeInterface {
    $this->id = $id;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setLabel(string $label): OfficeInterface {
    $this->label = $label;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setCode(string $code): OfficeInterface {
    $this->code = $code;
    return $this;
  }


  /**
   * @inheritDoc
   */
  public function setAddress(string $address): OfficeInterface {
    $this->address = $address;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setNui(string $nui): OfficeInterface {
    $this->nui = $nui;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setCurrencyId(string $currency_id): OfficeInterface {
    $this->currency_id = $currency_id;
    return $this;
  }


}
