<?php declare(strict_types=1);

namespace Drupal\taller_solid\Entity;

use Drupal\taller_solid\Contracts\Entities\CurrencyInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the currency entity type.
 *
 * @ConfigEntityType(
 *   id = "currency",
 *   label = @Translation("currency"),
 *   label_collection = @Translation("currencies"),
 *   label_singular = @Translation("currency"),
 *   label_plural = @Translation("currencies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count currency",
 *     plural = "@count currencies",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\taller_solid\CurrencyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\taller_solid\Form\CurrencyForm",
 *       "edit" = "Drupal\taller_solid\Form\CurrencyForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "currency",
 *   admin_permission = "administer currency",
 *   links = {
 *     "collection" = "/admin/structure/currency",
 *     "add-form" = "/admin/structure/currency/add",
 *     "edit-form" = "/admin/structure/currency/{currency}",
 *     "delete-form" = "/admin/structure/currency/{currency}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "currency_code",
 *     "country",
 *     "description",
 *   },
 * )
 */
final class Currency extends ConfigEntityBase implements CurrencyInterface {

  /**
   * The currency ID.
   */
  protected string $id;

  /**
   * The currency label.
   */
  protected string $label;

  /**
   * The currency code.
   */
  protected $currency_code;

  /**
   * The currency country.
   */
  protected string $country;

  /**
   * The currency description.
   */
  protected string $description;

  /**
   * @inheritDoc
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @inheritDoc
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * @inheritDoc
   */
  public function getCurrencyCode(): string {
    return $this->currency_code;
  }

  /**
   * @inheritDoc
   */
  public function getCountry(): string {
    return $this->country;
  }

  /**
   * @inheritDoc
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * @inheritDoc
   */
  public function setId(string $id): CurrencyInterface {
    $this->id = $id;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setLabel(string $label): CurrencyInterface {
    $this->label = $label;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setCurrencyCode(string $currency_code): CurrencyInterface {
    $this->currency_code = $currency_code;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setCountry(string $country): CurrencyInterface {
    $this->country = $country;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function setDescription(string $description): CurrencyInterface {
    $this->description = $description;
    return $this;
  }

}
