<?php declare(strict_types=1);

namespace Drupal\taller_solid\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\taller_solid\Contracts\RestLogic\OfficeRestLogicInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Represents Currency Rest records as resources.
 *
 * @RestResource (
 *   id = "taller_solid_office_rest",
 *   label = @Translation("Office Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/information/office",
 *     "create" = "/api/v1/information/office"
 *   }
 * )
 *
 */
final class OfficeRestResource extends ResourceBase {

  /**
   * @var \Drupal\taller_solid\Contracts\RestLogic\OfficeRestLogicInterface
   */
  protected OfficeRestLogicInterface $office_logic_service;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $request;

  /**
   * @inheritDoc
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->office_logic_service = $container->get('taller_solid.v1.office_rest_logic');
    $instance->request = $container->get('request_stack');
    return $instance;
  }

  /**
   * @return \Drupal\rest\ModifiedResourceResponse
   */
  public function get(): ModifiedResourceResponse {
    $office_id = $this->request->getCurrentRequest()->get('id');
    try {
      if (isset($office_id)) {
        $resource = $this->office_logic_service->getOffice($office_id);
      }
      else {
        $resource = $this->office_logic_service->getAllOffices();
      }
    }
    catch (\Exception $e) {
      $resource = ['error' => $e->getMessage(), 'code' => $e->getCode()];
    }

    return new ModifiedResourceResponse($resource);
  }

  /**
   * @param array $data
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   */
  public function post(array $data): ModifiedResourceResponse {
    try {
      $resource = $this->office_logic_service->createOffice($data);
    }
    catch (\Exception $e) {
      $resource = ['error' => $e->getMessage(), 'code' => $e->getCode()];
    }
    return new ModifiedResourceResponse($resource);
  }

  /**
   * @param array $data
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   */
  public function patch(array $data): ModifiedResourceResponse {
    try {
      $office_id = $this->request->getCurrentRequest()->get('id');
      $resource = $this->office_logic_service->updateOffice($office_id,$data);
    } catch (\Exception $e) {
      $resource = ['error' => $e->getMessage(), 'code' => $e->getCode()];
    }
    return new ModifiedResourceResponse($resource);
  }

  /**
   * @return \Drupal\rest\ModifiedResourceResponse
   */
  public function delete(): ModifiedResourceResponse {
    $office_id = $this->request->getCurrentRequest()->get('id');
    try {
      $resource =  $this->office_logic_service->deleteOffice($office_id);
    }
    catch (\Exception $e) {
      $resource = ['error' => $e->getMessage(), 'code' => $e->getCode()];
    }

    return (new ModifiedResourceResponse(null, 200))
      ->setContent(Json::encode($resource));
  }

}
