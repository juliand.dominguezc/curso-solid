## Taller S.O.L.I.D Odoo I+D+I

Este repositorio contiene el código fuente del módulo de drupal donde se realiza
el desarrollo del taller final del curso de S.O.L.I.D.

## REQUERIMIENTOS

- Drupal 10
- PHP 8.1.x o superior

## PROPIETARIO
Este taller es realizado por: Julian David Dominguez

